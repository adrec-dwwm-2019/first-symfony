<?php

namespace App\Controller\Front;

use App\Entity\Movie;
use App\Entity\Post;


use App\Repository\MovieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * HomeController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @Route("/", name="home")
     * @param MovieRepository $movieRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(MovieRepository $movieRepository)
    {
        $movies = $movieRepository->findAll();

        return $this->render('Front/Pages/home/index.html.twig', [
            'movies' => $movies,
        ]);
    }
}
