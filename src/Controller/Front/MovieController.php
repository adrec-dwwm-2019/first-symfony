<?php

namespace App\Controller\Front;

use App\Entity\Movie;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class MovieController extends AbstractController
{
    /**
     * @Route("/movie/{id}", name="movie_detail")
     * @param Movie $movie
     * @param PostRepository $postRepository
     * @return Response
     */
    public function index(Movie $movie, PostRepository $postRepository)
    {
        return $this->render('Front/Pages/movie/index.html.twig', [
            'movie' => $movie,
            'related_posts' => $postRepository->findByMovie($movie),
        ]);
    }
}
