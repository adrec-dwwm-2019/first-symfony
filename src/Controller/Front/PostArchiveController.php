<?php

namespace App\Controller\Front;

use App\Repository\PostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class PostArchiveController extends AbstractController
{
    /**
     * @Route("/articles", name="post_archive")
     * @param PaginatorInterface $paginator
     * @param PostRepository $postRepository
     * @param Request $request
     * @return Response
     */
    public function index(
        PaginatorInterface $paginator,
        PostRepository $postRepository,
        Request $request
    )
    {

        $posts = $paginator->paginate(
            $postRepository->findBy(
                [],
                ['createdAt' => 'DESC']
            ), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            12 /*limit per page*/
        );


        return $this->render('Front/Pages/post_archive/index.html.twig', [
           'posts' => $posts,
        ]);
    }
}
