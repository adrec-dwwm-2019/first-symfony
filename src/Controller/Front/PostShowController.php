<?php

namespace App\Controller\Front;

use App\Entity\Post;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class PostShowController extends AbstractController
{
    /**
     * @Route("/article/{id}", name="post_detail")
     * @param Post $post
     * @return Response
     */
    public function index(Post $post, PostRepository $postRepository)
    {
        return $this->render('Front/Pages/post_show/index.html.twig', [
            'post' => $post,
            'related_posts' => $postRepository->findByMovie($post->getMovie())
        ]);
    }
}
