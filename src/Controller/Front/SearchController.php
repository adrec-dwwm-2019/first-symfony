<?php

namespace App\Controller\Front;

use App\Form\SearchType;
use App\Repository\MovieRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/recherche", name="search")
     * @param Request $request
     * @param MovieRepository $movieRepository
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(
        Request $request,
        MovieRepository $movieRepository,
        PaginatorInterface $paginator
    )
    {
        $movies = [];

        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);

        $requestData = $request->get('search');

        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            dump($data);
        }


        if($requestData != null) {
            $search = '';

            if(!empty($data['search'])) {
                $search = $data['search'];
            } elseif (!empty($requestData['search'])) {
                $search = $requestData['search'];
            }

            $movies = $paginator->paginate(
                $movieRepository->getSearchByTitleQB($search),
                $request->query->getInt('page', 1), /*page number*/
                2 /*limit per page*/
            );
        }


        return $this->render('Front/Pages/search/index.html.twig', [
            'form' => $form->createView(),
            'movies' => $movies,
        ]);
    }
}
