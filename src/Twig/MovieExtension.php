<?php


namespace App\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;


class MovieExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('hourDurationDisplay', [$this, 'hourDurationDisplay']),
            new TwigFilter('getExcerpt', [$this, 'getExcerpt']),
        ];
    }

    public function hourDurationDisplay(string $duration)
    {

        return date('H:i', mktime(0,$duration));
    }


    /**
     * @param string $content
     * @param int $limit
     * @param string $suffix
     * @return string
     */
    public function getExcerpt(string $content, int $limit = 20, string $suffix = '...'): string
    {
        //Remove html form content
        $content = strip_tags($content);

        //Remove \n and \r form content
        $content = str_replace(["\n", "\r"], ' ', $content);

        $wordArray = explode(' ', $content);

        $excerpt = implode(' ', array_slice($wordArray, 0, $limit));

        if (count($wordArray) > $limit) {
            $excerpt .= ' ' . $suffix;
        }


        return $excerpt;
    }

}

