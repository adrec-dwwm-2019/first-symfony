# Premier symfony

Premier symfo en cours avec les DWWM 2019


## Installation

Voici la liste des commandes à exécuter pour lancer le projet

	composer install
	yarn 
Générer les fixtures

    php bin/console hautelook:fixtures:load --purge-with-truncate -n
Pour lancer le watch

	yarn watch
Pour lancer le serveur

    php bin/console server:run
## Quelques commandes utiles
Liste des routes disponible

    php bin/console debug:router
Vider le cache 
	
	php bin/console cache:clear
Créer la base de données

    php bin/console doctrine:database:create
Pour créer ou modifier une entité
    
    php bin/console make:entity
Faire une migration pour mettre à jour la bdd

    php bin/console make:migration
Pour appliquer les migrations : 

    php bin/console doctrine:migrations:migrate